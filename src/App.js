import React, {Component} from 'react';
import ReactDOM from 'react-dom'
import 'bootstrap/dist/css/bootstrap.css'
import Modal from "./components/Modal";

class App extends Component {

    state = {
        myComputers: [],
        modalVisible: false,
        currentItem: ''
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevState.myComputers.length < this.state.myComputers) {
            alert('Added successfully!');
        }
    }

    componentDidMount() {

        const computers = [
            {
                name: "Comp_1",
                cpu: 2,
                ram: 8,
                ssd: 128
            },
            {
                name: "Comp_2",
                cpu: 4,
                ram: 16,
                ssd: 128
            },
            {
                name: "Comp_3",
                cpu: 4,
                ram: 8,
                ssd: 512
            },
            {
                name: "Comp_4",
                cpu: 2,
                ram: 16,
                ssd: 256
            }
        ]

        this.setState({
            myComputers: computers
        })
    }

    removeComputer = (index) => {
        let myComputers = this.state.myComputers;
        myComputers.splice(index, 1);

        this.setState({
            myComputers: myComputers
        })
    }

    openModal = () => {
        this.setState({
            modalVisible: true
        })

    }

    hideModal = () => {
        this.setState({
            modalVisible: false
        })

    }

    changeCurrentItem = (type, isIncrement) => {
        let a = this.state.currentItem ? this.state.currentItem : {name: 'Comp_4', cpu: 0, ram: 0, ssd: 0};

        if (type === 'cpu') {
            if (!isIncrement) {
                if (a.cpu !== 0)
                    a.cpu -= 2;
            } else {
                a.cpu += 2
            }


        }

        if (type === 'ram') {
            if (!isIncrement) {
                if (a.ram !== 0)
                    a.ram -= 2
            } else {
                a.ram += 2
            }
        }

        if (type === 'ssd') {
            if (!isIncrement) {
                if (a.ssd !== 0)
                    a.ssd -= 128;
            } else {
                a.ssd += 128
            }
        }

        this.setState({
            currentItem: a
        })
    }

    saveComputer = () => {
        const {myComputers, currentItem} = this.state
        currentItem.name = 'Comp_' + (myComputers.length + 1)
        let clone = [...myComputers]
        clone.push(currentItem)
        this.setState({
            myComputers: clone,
            modalVisible: false
        })
    }

    clearCurrentItem = () => {
        this.setState({
            currentItem: ''
        })
    }

    render() {

        const {myComputers, modalVisible, currentItem} = this.state

        return (
            <div className={'container'}>

                <h1>My Computers</h1>

                <div className="row mt-3">
                    <div className="col-md-8">
                        {
                            this.state.modalVisible ?
                                <Modal clearCurrentItem={this.clearCurrentItem}
                                       saveComputer={this.saveComputer}
                                       changeCurrentItem={this.changeCurrentItem}
                                       currentItem={currentItem}
                                       hideModal={this.hideModal}/> : ""
                        }
                    </div>
                    <div className="col-md-4 text-right">
                        <button className="btn btn-success" onClick={this.openModal}>Add</button>
                    </div>

                </div>

                <div className="row mt-4">
                    <div className="col-md-12">
                        <table className="table table-striped">
                            <thead>
                            <tr>
                                <th>N</th>
                                <th>Name</th>
                                <th>CPU</th>
                                <th>RAM</th>
                                <th>SSD</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            {myComputers.map((item, index) => <tr key={index}>
                                <td>{index + 1}</td>
                                <td>{item.name}</td>
                                <td>{item.cpu}</td>
                                <td>{item.ram}</td>
                                <td>{item.ssd}</td>
                                <td>
                                    <button onClick={() => this.removeComputer(index)}
                                            className="btn btn-danger">remove
                                    </button>
                                </td>
                            </tr>)}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        );
    }
}

export default App

ReactDOM.render(
    <App/>, document.getElementById('root')
)
import React, {Component} from 'react';

class Modal extends Component {

    componentWillUnmount() {
        this.props.clearCurrentItem()
    }

    onCancelClicked = () => {
        this.props.hideModal()
    }

    changeCurrentItem = (type, isIncrement) => {
        this.props.changeCurrentItem(type, isIncrement)
    }

    saveComputer = () => {
        this.props.saveComputer()
    }

    render() {

        const {currentItem} = this.props

        return (
            <div className={'card'}>
                <div className="card-header">
                    <h4>Add computer</h4>
                </div>
                <div className="card-body">
                    <div className="row">
                        <div className="col-md-4">
                            <button onClick={() => this.changeCurrentItem('cpu', false)}
                                    className={'btn btn-danger font-weight-bold'}>-
                            </button>
                            <b className={'font-weight-bold p-2'}>CPU : {currentItem ? currentItem.cpu : 0}</b>
                            <button onClick={() => this.changeCurrentItem('cpu', true)}
                                    className={'btn btn-success font-weight-bold'}>+
                            </button>
                        </div>
                        <div className="col-md-4">
                            <button onClick={() => this.changeCurrentItem('ram', false)}
                                    className={'btn btn-danger font-weight-bold'}>-
                            </button>
                            <b className={'font-weight-bold p-2'}>RAM : {currentItem ? currentItem.ram : 0}</b>
                            <button onClick={() => this.changeCurrentItem('ram', true)}
                                    className={'btn btn-success font-weight-bold'}>+
                            </button>
                        </div>
                        <div className="col-md-4">
                            <button onClick={() => this.changeCurrentItem('ssd', false)}
                                    className={'btn btn-danger font-weight-bold'}>-
                            </button>
                            <b className={'font-weight-bold p-2'}>SSD : {currentItem ? currentItem.ssd : 0}</b>
                            <button onClick={() => this.changeCurrentItem('ssd', true)}
                                    className={'btn btn-success font-weight-bold'}>+
                            </button>
                        </div>
                    </div>
                </div>
                <div className="card-footer">
                    <button className="btn btn-success" onClick={this.saveComputer}>Save</button>
                    <button className="btn btn-danger ml-2" onClick={this.onCancelClicked}>Cancel</button>
                </div>
            </div>
        );
    }
}

export default Modal;